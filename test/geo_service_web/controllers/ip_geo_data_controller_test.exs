defmodule GeoServiceWeb.IpGeoDataControllerTest do
  use GeoServiceWeb.ConnCase

  setup do
    Ecto.Adapters.SQL.Sandbox.checkout(GeoService.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(GeoService.Repo, {:shared, self()})

    GeoService.IpGeoDataImporter.import("files/test_data_dump.csv")
  end

  test "gets ip info", %{conn: conn} do
    conn = get(conn, Routes.ip_geo_data_path(conn, :show, "200.106.141.15"))

    assert %{
             "city" => "DuBuquemouth",
             "country" => "Nepal",
             "country_code" => "SI",
             "ip_address" => "200.106.141.15",
             "latitude" => -84.87503094689836,
             "longitude" => 7.206435933364332,
             "mistery_value" => "7823011346"
           } = json_response(conn, 200)["data"]
  end

  test "gets not found error", %{conn: conn} do
    conn = get(conn, Routes.ip_geo_data_path(conn, :show, "0.0.0.0"))

    assert %{"error" => "IP \"0.0.0.0\" not found"} = json_response(conn, 404)
  end
end
