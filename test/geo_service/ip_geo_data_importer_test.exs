defmodule GeoService.IpGeoDataImporterTest do
  use ExUnit.Case
  alias GeoService.IpGeoDataImporter
  alias GeoService.Repo
  alias GeoService.Schema.IpGeoData

  doctest IpGeoDataImporter

  test "imports file content to database" do
    assert %{time_elapsed_us: _, accepted: 2, rejected: 2} =
             IpGeoDataImporter.import("files/test_data_dump.csv")

    assert [
             %GeoService.Schema.IpGeoData{
               __meta__: _,
               city: "DuBuquemouth",
               country: "Nepal",
               country_code: "SI",
               id: _,
               ip_address: %Postgrex.INET{address: {200, 106, 141, 15}, netmask: 32},
               latitude: -84.87503094689836,
               longitude: 7.206435933364332,
               mistery_value: "7823011346"
             },
             %GeoService.Schema.IpGeoData{
               __meta__: _,
               city: "Gradymouth",
               country: "Saudi Arabia",
               country_code: "TL",
               id: _,
               ip_address: %Postgrex.INET{address: {70, 95, 73, 73}, netmask: 32},
               latitude: -49.16675918861615,
               longitude: -86.05920084416894,
               mistery_value: "2559997162"
             }
           ] = Enum.sort(Repo.all(IpGeoData))
  end
end
