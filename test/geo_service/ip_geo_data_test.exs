defmodule GeoService.IpGeoDataTest do
  use ExUnit.Case

  alias GeoService.{IpGeoData, IpGeoDataImporter}

  setup do
    :ok = Ecto.Adapters.SQL.Sandbox.checkout(GeoService.Repo)
    Ecto.Adapters.SQL.Sandbox.mode(GeoService.Repo, {:shared, self()})
  end

  test "returns geo info for given ip address" do
    assert %{time_elapsed_us: _, accepted: 2, rejected: 2} =
             IpGeoDataImporter.import("files/test_data_dump.csv")

    assert %GeoService.Schema.IpGeoData{
             __meta__: _,
             city: "DuBuquemouth",
             country: "Nepal",
             country_code: "SI",
             id: _,
             ip_address: %Postgrex.INET{address: {200, 106, 141, 15}, netmask: 32},
             latitude: -84.87503094689836,
             longitude: 7.206435933364332,
             mistery_value: "7823011346"
           } = IpGeoData.info("200.106.141.15")
  end
end
