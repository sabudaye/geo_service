# GeoService
Service for getting (fake) geographical information for a given IP address

# Requirements
- Docker
- 4Gb of free memory

# Usage
- `docker-compose up`
- `curl http://localhost/api/200.106.141.15`

```
{"error":"IP \"200.106.141.15\" not found"}
```

# API
Service has only one endpoint - GET `/api/<IP address>`

# Seeding with data
- copy `data_dump.csv` file into `./volumes/seed_data/data_dump.csv` inside project folder
- `docker ps`
- `docker exec -it geo_service_app_1 /bin/bash`
- `bin/geo_service rpc "GeoService.Release.import"`
- `curl http://localhost/api/200.106.141.15`

```
{"data":{"city":"DuBuquemouth","country":"Nepal","country_code":"SI","ip_address":"200.106.141.15","latitude":-84.87503094689836,"longitude":7.206435933364332,"mistery_value":"7823011346"}}
```

# Running tests locally
- Install elixir 1.10.4
- `docker-compose down`
- `docker-compose -f docker-compose.test.yml up`
- `mix deps.get`
- `mix ecto.setup`
- `mix test`

# Example of running applicatiion
http://178.62.214.31/api/200.106.141.15
