defmodule GeoService.Repo.Migrations.CreateIpGeoData do
  use Ecto.Migration

  def change do
    create table(:ip_geo_data) do
      add :ip_address, :inet, null: false
      add :country_code, :string, null: false
      add :country, :string, null: false
      add :city, :string, null: false
      add :latitude, :float, null: false
      add :longitude, :float, null: false
      add :mistery_value, :string, null: false
    end

    create unique_index(:ip_geo_data, [:ip_address])
  end
end
