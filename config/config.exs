# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :geo_service,
  ecto_repos: [GeoService.Repo]

# Configures the endpoint
config :geo_service, GeoServiceWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "bw7CGzFGinVynbV2BqAdUv1lwVb3PodPC65QFinozT3NqiYAKhOF2kfe0UMtghNz",
  render_errors: [view: GeoServiceWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: GeoService.PubSub,
  live_view: [signing_salt: "Fc/jb0B9"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
