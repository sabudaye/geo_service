defmodule GeoService.IpGeoDataImporter do
  @moduledoc """
  Module to parse ip geo data csv files
  """
  alias NimbleCSV.RFC4180, as: CSV
  alias Flow.Window, as: FlowWindow
  alias EctoNetwork.INET, as: EctoNetworkInet
  alias GeoService.Repo
  alias GeoService.Schema.IpGeoData

  @type ip_geo_data :: %{
          ip_address: tuple(),
          country_code: String.t(),
          country: String.t(),
          city: String.t(),
          latitude: number(),
          longitude: number(),
          mistery_value: String.t()
        }
  @type ip_geo_data_import_stats :: %{
          time_elapsed: pos_integer(),
          rejected: non_neg_integer(),
          accepted: non_neg_integer()
        }

  @accepted_counter_idx 1
  @rejected_counter_idx 2

  @spec import(Path.t()) :: ip_geo_data_import_stats()
  @spec import(Path.t(), Ecto.Repo.t(), Ecto.Schema.t()) :: ip_geo_data_import_stats()
  @doc """
  Parses ip geo data csv file from given path and imports valid content to given Ecto.Repo schema
  Returns statistics about elapsed time and amount of data accepted or rejected

  ## Examples

      iex> result = GeoService.IpGeoDataImporter.import("files/test_data_dump.csv")
      iex> %{time_elapsed_us: _, accepted: 2, rejected: 2} = result
  """
  def import(path, repo \\ Repo, schema \\ IpGeoData, timeout \\ 60_000) do
    task = Task.async(fn -> import_with_stats(path, repo, schema) end)
    Task.await(task, timeout)
  end

  defp import_with_stats(path, repo, schema) do
    event(:start_parsing)

    # counter 1 for accepted rows
    # counter 2 for rejected rows
    counter_ref = :counters.new(2, [:atomics])

    result =
      path
      |> parse_raw()
      |> Flow.reject(&reject_invalid_counted(&1, counter_ref))
      |> Flow.uniq_by(&uniq_by_ip/1)
      |> Flow.partition(window: FlowWindow.count(1_000))
      |> Flow.reduce(fn -> [] end, fn item, batch -> [item | batch] end)
      |> Flow.on_trigger(&insert_all(repo, schema, &1))
      |> Enum.to_list()

    length = length(result)
    accepted = :counters.get(counter_ref, @accepted_counter_idx)
    rejected = :counters.get(counter_ref, @rejected_counter_idx) + accepted - length

    event(:end_parsing)

    %{
      rejected: rejected,
      accepted: length,
      time_elapsed_us: :timer.now_diff(timing(:end_parsing), timing(:start_parsing))
    }
  end

  defp insert_all(repo, schema, items) do
    new_items = Enum.map(items, &cast_ip_addr/1)

    repo.insert_all(
      schema,
      new_items,
      on_conflict: :replace_all,
      conflict_target: :ip_address
    )

    {Enum.map(items, & &1.ip_address), items}
  end

  defp cast_ip_addr(item) do
    {:ok, ip_addr} = EctoNetworkInet.cast(item.ip_address)
    Map.put(item, :ip_address, ip_addr)
  end

  defp parse_raw(path) do
    path
    |> File.stream!(read_ahead: 100_000)
    |> CSV.parse_stream()
    |> Flow.from_enumerable()
    |> Flow.partition()
    |> Flow.map(fn [ip_addr, c_code, country, city, lat, lon, m_value] ->
      %{
        ip_address: parse_ip(ip_addr),
        country_code: parse_str(c_code),
        country: parse_str(country),
        city: parse_str(city),
        latitude: parse_lat_lon(lat, 90),
        longitude: parse_lat_lon(lon, 180),
        mistery_value: m_value
      }
    end)
  end

  defp uniq_by_ip(%{ip_address: ip_addr}), do: ip_addr

  defp reject_invalid(geo_data), do: Enum.any?(geo_data, fn {_k, v} -> v == :invalid end)

  defp reject_invalid_counted(geo_data, counter_ref) do
    case Enum.any?(geo_data, fn {_k, v} -> v == :invalid end) do
      true ->
        :counters.add(counter_ref, @rejected_counter_idx, 1)
        true

      false ->
        :counters.add(counter_ref, @accepted_counter_idx, 1)
        false
    end
  end

  defp parse_str(""), do: :invalid
  defp parse_str(str) when is_binary(str), do: str
  defp parse_str(_), do: :invalid

  defp parse_ip(addr_str) do
    with addr_charlist <- String.to_charlist(addr_str),
         {:ok, addr_tuple} <- :inet.parse_address(addr_charlist) do
      addr_tuple
    else
      _ -> :invalid
    end
  end

  defp parse_lat_lon(lat_str, max) do
    with lat_int <- String.to_float(lat_str),
         true <- lat_int >= -max,
         true <- lat_int <= max do
      lat_int
    else
      _ -> :invalid
    end
  rescue
    _ -> :invalid
  end

  defp event(key), do: :erlang.put({:time, key}, :os.timestamp())

  defp timing(key), do: :erlang.get({:time, key})
end
