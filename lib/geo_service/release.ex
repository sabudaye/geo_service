defmodule GeoService.Release do
  @moduledoc """
  Tasks to be run through mix release
  """
  @app :geo_service

  def migrate do
    for repo <- repos() do
      {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :up, all: true))
    end
  end

  def rollback(repo, version) do
    {:ok, _, _} = Ecto.Migrator.with_repo(repo, &Ecto.Migrator.run(&1, :down, to: version))
  end

  def import(path \\ System.get_env("PATH_TO_SEED_DATA")) do
    Application.load(@app)
    GeoService.IpGeoDataImporter.import(path)
  end

  defp repos do
    Application.load(@app)
    Application.fetch_env!(@app, :ecto_repos)
  end
end
