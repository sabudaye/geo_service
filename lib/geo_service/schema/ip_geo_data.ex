defmodule GeoService.Schema.IpGeoData do
  @moduledoc """
  Ip Geo Data database schema
  """
  use Ecto.Schema

  schema "ip_geo_data" do
    field :ip_address, EctoNetwork.INET
    field :country_code, :string
    field :country, :string
    field :city, :string
    field :latitude, :float
    field :longitude, :float
    field :mistery_value, :string
  end
end
