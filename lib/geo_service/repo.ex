defmodule GeoService.Repo do
  use Ecto.Repo,
    otp_app: :geo_service,
    adapter: Ecto.Adapters.Postgres
end
