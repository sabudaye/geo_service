defmodule GeoService.IpGeoData do
  @moduledoc """
  GeoSrvice.IpGeoData provides functionality to get geographical information for given IP address
  """
  alias GeoService.Repo
  alias GeoService.Schema.IpGeoData
  import Ecto.Query

  def info(ip_addr) when is_binary(ip_addr) do
    case get_info(ip_addr) do
     info when not is_nil(info) -> info
      _ -> {:error, "IP #{inspect(ip_addr)} not found"}
    end
  end

  defp get_info(ip_addr) do
    Repo.one(from(i in IpGeoData, where: i.ip_address == ^ip_addr))
  end
end
