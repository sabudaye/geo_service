defmodule GeoServiceWeb.IpGeoDataView do
  use GeoServiceWeb, :view
  alias __MODULE__

  def render("show.json", %{ip_info: ip_info}) do
    %{data: render_one(ip_info, IpGeoDataView, "ip_info.json")}
  end

  def render("ip_info.json", %{ip_geo_data: ip_info}) do
    %{
      ip_address: addr_to_string(ip_info.ip_address),
      country_code: ip_info.country_code,
      country: ip_info.country,
      city: ip_info.city,
      latitude: ip_info.latitude,
      longitude: ip_info.longitude,
      mistery_value: ip_info.mistery_value
    }
  end

  defp addr_to_string(%{address: ip_addr}) do
    ip_addr |> Tuple.to_list() |> Enum.join(".")
  end
end
