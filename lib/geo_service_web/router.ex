defmodule GeoServiceWeb.Router do
  use GeoServiceWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", GeoServiceWeb do
    pipe_through :api

    get "/:ip", IpGeoDataController, :show
  end
end
