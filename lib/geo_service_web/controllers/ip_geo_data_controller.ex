defmodule GeoServiceWeb.IpGeoDataController do
  use GeoServiceWeb, :controller
  alias GeoService.IpGeoData

  @spec show(Plug.Conn.t(), map) :: Plug.Conn.t()
  def show(conn, %{"ip" => ip}) do
    case IpGeoData.info(ip) do
      {:error, message} ->
        conn
        |> put_status(:not_found)
        |> json(%{error: message})

      result ->
        render(conn, "show.json", %{ip_info: result})
    end
  end
end
